denkmal-website-admin
=====================

Administration for Denkmal.org


Development
-----------

Install dependencies:
```
yarn install
```

Run development server, with auto-reload:
```
yarn start
```


Production Deployment
---------------------

Gitlab CI is deploying the application to Netlify:
https://app.netlify.com/sites/serene-sinoussi-bc60af/overview
