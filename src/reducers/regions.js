export default (previousState = null, { type, regions }) => {
    if (type === 'SET_REGIONS') {
        return {
            ...regions
        };
    }
    return previousState;
}