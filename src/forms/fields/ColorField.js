import React from 'react';

export const ColorField = ({ source, record = {} }) => {
    const value = record[source];
    if (!value) {
        return null;
    }
    const htmlColor = '#' + record[source];
    return <div style={{backgroundColor: htmlColor, width: '100%', height: 20}}></div>;
}